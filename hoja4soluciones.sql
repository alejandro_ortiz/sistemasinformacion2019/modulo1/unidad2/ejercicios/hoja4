﻿USE ciclistas;

-- 1. Nombre y edad de los ciclistas que han ganado etapas

  SELECT 
    DISTINCT c.nombre, c.edad 
    FROM etapa e 
      JOIN ciclista c USING (dorsal);

-- 2. Nombre y edad de los ciclistas que han ganado puertos

  SELECT 
    DISTINCT c.nombre, c.edad 
    FROM puerto p 
      JOIN ciclista c USING (dorsal);

-- 3. Nombre y edad de los ciclistas que han ganado etapas y puertos

  SELECT 
    DISTINCT c.nombre, c.edad 
    FROM (etapa e 
      JOIN ciclista c USING (dorsal)) 
        JOIN puerto p USING (dorsal);

-- 4. Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa

  SELECT 
    DISTINCT e1.director 
    FROM (etapa e 
      JOIN ciclista c USING (dorsal)) 
        JOIN equipo e1 USING (nomequipo); 

-- 5. Dorsal y nombre de los ciclistas que hayan llevado algún maillot

  SELECT 
    DISTINCT c.dorsal, c.nombre 
    FROM ciclista c 
      JOIN lleva l USING (dorsal);

-- 6. Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo

  SELECT 
    DISTINCT c.dorsal, c.nombre 
    FROM ciclista c 
      JOIN lleva l USING (dorsal) 
      WHERE l.código='MGE';

-- 7. Dorsal de los ciclistas que hayan llevado algún maillot y que han ganado etapas

  SELECT 
    DISTINCT c.dorsal 
    FROM (ciclista c 
      JOIN lleva l USING (dorsal)) 
        JOIN etapa e USING (dorsal);

-- 8. Indicar el numetapa de las etapas que tengan puertos

  SELECT 
    DISTINCT e.numetapa 
    FROM etapa e 
      JOIN puerto p USING (numetapa);

-- 9. Indicar los km de las etapas que hayan ganado ciclistas del Banesto y tengan puertos

  SELECT 
    DISTINCT e.kms 
    FROM (etapa e 
      JOIN ciclista c USING (dorsal)) 
        JOIN puerto p USING (numetapa) 
        WHERE c.nomequipo='Banesto';

-- 10. Listar el número de ciclistas que hayan ganado alguna etapa con puerto

  SELECT 
    COUNT(DISTINCT e.dorsal) ciclistas_ganado 
    FROM etapa e 
      JOIN puerto p USING (numetapa);  

-- 11. Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto

  SELECT 
    p.nompuerto 
    FROM puerto p 
      JOIN ciclista c USING (dorsal) 
      WHERE c.nomequipo='Banesto';

-- 12. Listar el número de etapas que tengan puerto que hayan sido ganados por ciclistas de Banesto con más de 200 km

  SELECT 
    COUNT(DISTINCT e.numetapa)etapas 
    FROM (etapa e 
      JOIN ciclista c USING (dorsal)) 
        JOIN puerto p USING (numetapa) 
        WHERE c.nomequipo='Banesto' 
          AND e.kms>200;